const async = require("async");
const http = require("http"),
	urls = process.argv.slice(2),
	bl = require("bl");


async.filter(urls, doGet, function (results) {
	console.log(results);
});

function doGet(url, callback) {
	http.get(url,function (response) {
//		response.setEncoding("utf8");
		response.on("data", console.log);
		response.on("end", function () {
			callback(true);
		});
	}).on("error", function (err) {
			callback(false);
		});
//	return callback(null, url);
}