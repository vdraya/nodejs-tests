const after = require("after");
const http = require("http"),
	urls = process.argv.slice(2),
	bl = require("bl");


var work = after(urls.length, doSomething);
urls.forEach(function (url, i) {
	getUrl(url, i)
});


function getUrl(url, i) {
	http.get(url, function (response) {
		response.on("error", console.error);
		response.on("data", function (err, data) {

		});
		response.on("end", work);
	}).on("error", console.error);
}

function doSomething(arg, arg2) {
	console.log(arg, arg2);
}