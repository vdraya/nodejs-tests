var args = process.argv;

var fs = require("fs");
if (!args[2]) throw "You need to provide a valid file path";

fs.readFile(args[2], "utf-8", function (err, data) {
	if (err) throw err;
	var lines = data.split("\n").length - 1;
	console.log(lines);
});