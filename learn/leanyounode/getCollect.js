const http = require("http"),
	bl = require("bl"),
	url = process.argv[2];


http.get(url, function (response) {
	response.on("error", handleError);
	response.pipe(bl(handleReturn));
});

function handleError(err) {
	console.error(err);
}

handleReturn = function (err, data) {
	if (err) handleError(err);
	console.log(data.length);
	console.log(data.toString());
}