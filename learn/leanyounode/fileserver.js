const http = require("http"),
	fs = require("fs"),
	serverPort = process.argv[2],
	fileToProvide = process.argv[3];

var server = http.createServer(doRequest);
server.listen(serverPort);

function doRequest(request, response) {
	var fileStream = fs.createReadStream(fileToProvide);
	fileStream.on("error", function (err) {
		response.write(err.toString());
		response.end();
	});
	fileStream.pipe(response);
//	response.end();
}