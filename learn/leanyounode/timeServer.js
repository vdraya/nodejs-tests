const net = require("net"),
	port = process.argv[2],
	bl = require("bl");

var server = net.createServer(function (socket) {
	var returnContent = new bl();
	var date = new Date();
	returnContent.append(date.getFullYear().toString());
	returnContent.append("-");
	returnContent.append(zerofy(date.getMonth() + 1));
	returnContent.append("-");
	returnContent.append(zerofy(date.getDate()));
	returnContent.append(" ");
	returnContent.append(zerofy(date.getHours()))
	returnContent.append(":");
	returnContent.append(zerofy(date.getMinutes()));
	socket.end(returnContent.toString());
});

server.listen(port);


function zerofy(content, minLenght) {
	minLenght = Number(minLenght);
	if (isNaN(minLenght)) minLenght = 2;
	if((typeof content) != "string") content = content.toString();
	while (content.toString().length < minLenght) {
		content = "0" + content;
	}
	return content;
}