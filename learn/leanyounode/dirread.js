var path = process.argv[2];
var requiredExt = process.argv[3];
var fs = require("fs");
var pathTool = require("path");
if (!path || !requiredExt) throw "You need to provide the attributes (directory and required extension)";

if (!(requiredExt.indexOf(".") == 0)) requiredExt = "." + requiredExt;

fs.readdir(path, function (err, fileList) {
	if (err) throw err;

	fileList.forEach(function (file) {
		if (pathTool.extname(file) == requiredExt)
			console.log(file);
	});
});