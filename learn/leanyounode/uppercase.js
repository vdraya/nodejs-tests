const http = require("http"),
	map = require("through2-map"),
	serverPort = process.argv[2];

var server = http.createServer();
server.on("request", function (request, response) {
	if (request.method.toString() != "POST") {
		response.statusCode = 405;
		response.write("Unauthorized");
	}

	request.pipe(map(function (chunk) {
			return chunk.toString().toUpperCase();
		}
	)).pipe(response);
})

server.listen(serverPort);
