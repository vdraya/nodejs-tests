const http = require("http"),
	urls = process.argv.slice(2),
	bl = require("bl");

var running = urls.length;
var content = new Array();
urls.forEach(function (url, i) {
	http.get(url, function (response) {
		response.on("error", function () {
			console.error("errr");
		})
		response.pipe(bl(function (err, data) {
			content[i] = data.toString();
		}));
		response.on("end", done);
	});
});


function done() {
	if (--running !== 0) return;
	content.forEach(function (c) {
		console.log(c);
	});
}