module.exports = function (path, requiredExt, callback) {
	var fs = require("fs");
	var pathTool = require("path");

	if (!path || !requiredExt) return callback("You need to provide the attributes (directory and required extension)");

	if (!(requiredExt.indexOf(".") == 0)) requiredExt = "." + requiredExt;

	fs.readdir(path, function (err, fileList) {
		if (err) return callback(err);
		fileList = fileList.filter(function (file) {
			return pathTool.extname(file) === requiredExt;

		});
		return callback(null, fileList);
	});
};
