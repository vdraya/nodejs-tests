var args = process.argv;

var fs = require("fs");
if (!args[2]) throw "You need to provide a valid file path";

var buffer = fs.readFileSync(args[2]);
var str = buffer.toString().split("\n");
console.log(--str.length);
